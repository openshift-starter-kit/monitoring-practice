# ResourceQuota や Limitrange によるリソース管理
このセクションでは API リソースやシステムリソースを管理する方法として、[リソースクォータ (`ResourceQuota`)](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html/building_applications/_quotas) を用いて Project に対し、`Deployment` や `Service` などのリソースの件数や要求する CPU や Memory などのシステムリソースを制限します。
また [制限範囲 (`Limitrange`)](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html/nodes/nodes-cluster-limit-ranges) を用いて、Pod やコンテナにデフォルトで設定される CPU や Memory の使用量やリソースの上限を設定します。
`ResourceQuota` や `LimitRange` を利用することで、クラスタの物理的なシステムリソースやキャパシティを超えてユーザがシステムリソースを要求する様なケースを防ぐことを目的とします。

# Table of Contents
- [1. ResourceQuota によるシステムリソースの制限](#1-resourcequota-によるシステムリソースの制限)
  - [1.1. ResourceQuota の作成](#11-resourcequota-の作成)
  - [1.2. Pod の作成](#12-pod-の作成)
  - [1.3. ResourceQuota の利用状況](#13-resourcequota-の利用状況)
  - [1.4. CPU 上限の確認](#14-cpu-上限の確認)
- [2. ClusterResourceQuota によるシステムリソースの制限](#2-clusterresourcequota-によるシステムリソースの制限)
  - [2.1. ClusterResourceQuotaの作成](#21-clusterresourcequotaの作成)
  - [2.2. Pod の作成](#22-pod-の作成)
- [3. LimitRange によるシステムリソースの制限](#3-limitrange-によるシステムリソースの制限)
  - [3.1. LimitRange の作成](#31-limitrange-の作成)
  - [3.2. Pod の作成](#32-pod-の作成)
- [4. LimitRange によるデフォルト値の設定](#4-limitrange-によるデフォルト値の設定)
  - [4.1. ResourceQuota の作成](#41-resourcequota-の作成)
  - [4.2. LimitRange の作成](#42-limitrange-の作成)
  - [4.3. Pod の作成](#43-pod-の作成)

---
# 1. ResourceQuota によるシステムリソースの制限
`ResourceQuota` を作成し、Project で要求できるシステムリソースを制限します。
ここでは CPU を対象とし、設定値を超えてユーザが CPU コアを要求できないことを確認します。

## 1.1. ResourceQuota の作成
OpenShift Web Console からテスト用の Project を作成し、`ResourceQuota` を作成します。

OpenShift Web Console にログインし、規定の認証情報を入力します。
![handson](./images/resource_mgmt/login.png)  

左側メニューから **Home** → **Project** を選択します。  
![handson](./images/resource_mgmt/menu_project.png)

画面右側の **Create Project** を選択します。  
![handson](./images/resource_mgmt/create_project.png)

Name に `${USER}-capacity` を入力し、**Create** を選択します。
**`${USER}-` で始まる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
![handson](./images/resource_mgmt/create_userx_capacity.png)

Project が作成されることを確認します。
![handson](./images/resource_mgmt/project_detail.png)

左側メニューから **Administration** → **LimitRange** を選択し、デフォルトで作成される `LimitRange` を削除します。
`LimitRange` が設定されている場合、以降の `ResourceQuota` 作成後に Pod を作成することができないことを確認する箇所で作成することができるため、事前に `LimitRange` を削除します。
![handson](./images/resource_mgmt/limitrange.png)

左側メニューから **Administration** → **ResourceQuotas** を選択します。  
![handson](./images/resource_mgmt/menu_resourcequota.png)

左上の Project が `${USER}-capacity` であることを確認し、画面右上の **Create ResourceQuota** を選択します。  
![handson](./images/resource_mgmt/create_resourcequota.png)

デフォルトの設定を一部次のように修正し、**Create** を選択します。
* request.cpu : 500m
* limit.cpu : 1 

修正後は以下のようなマニフェストになります。  
![handson](./images/resource_mgmt/create_rq_example.png)

`ResourceQuota` が作成され、次のような統計情報が表示されます。
![handson](./images/resource_mgmt/rq_detail1.png)  

![handson](./images/resource_mgmt/rq_detail2.png)  

## 1.2. Pod の作成
Pod を作成して `ResourceQuota` による制限を確認します。

左側メニューから **Workloads** → **Pods** を選択します。  
![handson](./images/resource_mgmt/menu_pod.png)

画面右上の **Create Pod** を選択します。  
![handson](./images/resource_mgmt/create_pod.png)

`LimitRange` が設定されている場合、Pod を作成することができるため、事前に `LimitRange` を削除します。
Pod マニフェストを修正せずに **Create** を選択すると以下のようなエラーが出力され、Pod をデプロイすることはできません。
これは `ResourceQuota` に `Request` や `Limit` を設定した Project では `LimitRange` で Pod やコンテナに対する CPU や Memory のデフォルト値を設定しない限り、Pod をデプロイすることはできないためです。
![handson](./images/resource_mgmt/pod_create_error.png)

次に Pod をデプロイさせるため、マニフェストを以下のように書き換え、**Create** を選択します。
**`${USER}-` で始まる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
```
apiVersion: v1
kind: Pod
metadata:
  name: quota-cpu-400m
  labels:
    app: httpd
  namespace: ${USER}-capacity
spec:
  containers:
    - name: httpd
      image: 'image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest'
      ports:
        - containerPort: 8080
      resources:
        requests:
          cpu: 400m
          memory: 256Mi
        limits:
          cpu: 800m
          memory: 256Mi
```
![handson](./images/resource_mgmt/create_pod_cpu_400m.png)

Pod が正常に作成されることを確認します。  
![handson](./images/resource_mgmt/pod_detail.png)

## 1.3. ResourceQuota の利用状況
`ResourceQuota` で設定した上限に対し、現状でどれだけのリソースが要求されているか確認します。

左側メニューから **Administration** → **ResourceQuotas** を選択します。  
![handson](./images/resource_mgmt/menu_resourcequota.png)

`example` を選択します。  
![handson](./images/resource_mgmt/select_resourcequota.png)

ResourceQuota で設定した上限に対し、要求されているリソースの合計が表示されます。
例えば `cpu.request` が 500m に対し Pod の Request は 400m、`cpu.limits` が 1(1000m) に対し Pod の Limit は `800m` を設定したため、それぞれの使用率が 80% になっています。  
![handson](./images/resource_mgmt/rq_detail_after1.png)

スクロールダウンするとグラフ以外に数値として確認する事ができます。
![handson](./images/resource_mgmt/rq_detail_after1_2.png)

## 1.4. CPU 上限の確認
`ResourceQuota` の上限を超えるリソースを要求する Pod を作成し、`ResourceQuota` により Pod の作成が拒否されることを確認します。

左側メニューから **Workloads** → **Pods** を選択します。  
![handson](./images/resource_mgmt/menu_pod.png)  

画面右上の **Create Pod** を選択します。  
![handson](./images/resource_mgmt/create_pod_rq2.png)

現在の `cpu.requests` と `cpu.limits` の余剰はそれぞれ 100m と 200m になり、上限を超えるため Request に 300m と Limit に 600m を要求する Pod を作成します。  
マニフェストを以下のように作成し、**Create** を選択します。
**`${USER}-` で始まる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
```
apiVersion: v1
kind: Pod
metadata:
  name: quota-cpu-300m
  labels:
    app: httpd
  namespace: ${USER}-capacity
spec:
  containers:
    - name: httpd
      image: 'image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest'
      ports:
        - containerPort: 8080
      resources:
        requests:
          cpu: 300m
          memory: 256Mi
        limits:
          cpu: 600m
          memory: 256Mi
```

**Create** を選択すると以下のようなエラーが出力され、Pod をデプロイすることはできません。
"exceeded quota" と出力され、これは先の Pod が要求するリソースを合計すると `ResourceQuota` で設定した上限を超えるためです。
![handson](./images/resource_mgmt/create_pod_error2.png)

この Pod をデプロイするには既存の Pod を削除してリソースの余剰を増やすか、 `ResourceQuota` の上限を増やす必要があります。
ここでは特に対応はせず、次の検証に移ります。

`Deployment` を利用する場合、`spec.replicas` フィールドで Pod Replica 数を指定することができ、`Deployment` から作成される Pod 一つずつに対し指定したリソースが設定されるため、Pod Replicas 数に応じて 1 台目の Pod はデプロイされるが、2 台目以降はデプロイされないケースがあります。
これを確認します。

左側メニューから **Workloads**  → **Deployment** を選択します。  
![handson](./images/resource_mgmt/menu_deployment.png)

右上から **Create Deployment** を選択します。  
![handson](./images/resource_mgmt/create_deployment.png)  

デフォルトの設定から `cpu.requests` に 100m、`cpu.limits` に 200m に修正します。
**`${USER}-` で始まる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: deployment-cpu-100m
  namespace: ${USER}-capacity
spec:
  selector:
    matchLabels:
      app: httpd
  replicas: 3
  template:
    metadata:
      labels:
        app: httpd
    spec:
      containers:
        - name: httpd
          image: >-
            image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest
          ports:
            - containerPort: 8080
          resources:
            requests:
              cpu: 100m
              memory: 256Mi
            limits:
              cpu: 200m
              memory: 256Mi
```
![handson](./images/resource_mgmt/deployment_manifest.png)

**Create** を選択すると特にエラーがおきることなくデプロイされますが、Pod Replica 数に 3 を指定したにも関わらず 1 から増えることはありません。
![handson](./images/resource_mgmt/deployment_detail1.png)

スクロールダウンしてイベントを確認すると、**Conditions** セクションには "ReplicaFailure" により、リソース不足でスケールできてない旨のエラーが表示されています。  
![handson](./images/resource_mgmt/deployment_detail2.png)

現在 `cpu.requests` と `cpu.limits` の余剰はそれぞれ 100m と 200m となり、先程作成した `Deployment` から生成できる Pod は 1 台分だけのリソースがあることになり、余剰がなくなった結果、2 台目、3 台目の Pod はデプロイされませんでした。  
このように `Deployment` を使う際は要求する Pod Replica 数を満たすことができないケースがあります。

---
# 2. ClusterResourceQuota によるシステムリソースの制限
[`ClusterResourceQuota`](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html/building_applications/setting-quotas-across-multiple-projects) は複数の Project で共有して利用する `ResourceQuota` になります。
`ResourceQuota` は特定の Project に属すため、複数の Project で共通して横断的に利用する場合、各 Project に `ResourceQuota` を作成する必要があり、テンプレートの様な形で汎用的に利用することができません。
`ResourceQuota` を Project 毎に作成することはできますが、一括して設定を更新したい場合など運用面で煩雑であることが課題です。

## 2.1. ClusterResourceQuotaの作成
今回は複数の Project にまたがりリソースを制限するので、Project を 2 つ作成します。
左側メニューから **Home** → **Project** を選択します。  
![handson](./images/resource_mgmt/menu_project.png)

画面右側の **Create Project** を選択します。  
![handson](./images/resource_mgmt/create_project.png)

以下の内容を入力し、**Create** を選択します。
**`${USER}-` で始まる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
* Name : ${USER}-cap-crq-1
* Display name : 空欄のまま
* Description : ${USER}-crq
![handson](./images/resource_mgmt/create_userx_cap_crq_1.png)

同様の手順でも 2 つめの Project を作成します。  
* Name : ${USER}-cap-crq-2
* Display name : 空欄のまま
* Description : ${USER}-crq

![handson](./images/resource_mgmt/create_userx_cap_crq_2.png)

作成された Project の **YAML** を選択すると `metadata.annotations` に `openshift.io/description: ${USER}-crq` と設定されています。
この文字列を後ほど利用します。
![handson](./images/resource_mgmt/project_yaml.png)

次に `ClusterResourceQuota` を作成します。  
左側メニューの **Administration**  →  **CustomResourceDefinitions** を選択します。
![handson](./images/resource_mgmt/menu_crd.png)

サーチバーに "clusterresourcequota" と入力し、`ClusterResourceQuota` が表示されるのでこれを選択します。
![handson](./images/resource_mgmt/search_crd.png)

**Instances** を選択します。
![handson](./images/resource_mgmt/select_crq_instances.png)

 **Create ClusterResourceQuota** を選択します。
![handson](./images/resource_mgmt/select_create_crq.png)

マニフェストを以下のように作成し、**Create** を選択します。

**`${USER}-` で始まる文字列は `user1-` の様にログインするユーザ名に置き換えます。**

`selector.annotations` に Project を作成する際 Description に指定した `${USER}-crq` を指定します。
この Annotation により `ClusterResourceQuota` は対象とする Project を判定します。
```
apiVersion: quota.openshift.io/v1
kind: ClusterResourceQuota
metadata:
  name: ${USER}-crq
spec:
  quota:
    hard:
      requests.cpu: 500m
      limits.cpu: 1
      requests.memory: 1Gi
      limits.memory: 2Gi
  selector:
    annotations: 
      openshift.io/description: ${USER}-crq
```
![handson](./images/resource_mgmt/crq_manifest.png)

`ResourceQuota` と同じ様な統計情報が表示されます。
![handson](./images/resource_mgmt/crq_detail1.png)

## 2.2. Pod の作成
左側メニューから **Workloads** → **Pods** を選択します。  
![handson](./images/resource_mgmt/menu_pod.png)

左上の Project を `${USER}-cap-crq-1` に変更し、画面右上の **Create Pod** を選択します。  
![handson](./images/resource_mgmt/create_pod_cap_crq_1.png)
  
マニフェストを以下のように書き換え **Create** を選択します。
**`${USER}-` で始まる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
```
apiVersion: v1
kind: Pod
metadata:
  name: quota-cpu-400m
  labels:
    app: httpd
  namespace: ${USER}-cap-crq-1
spec:
  containers:
    - name: httpd
      image: 'image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest'
      ports:
        - containerPort: 8080
      resources:
        requests:
          cpu: 400m
          memory: 256Mi
        limits:
          cpu: 800m
          memory: 256Mi
```

![handson](./images/resource_mgmt/create_pod_cpu_400m_cap_crq_1.png)

Pod が作成されることを確認します。
![handson](./images/resource_mgmt/pod_detail_cpu_400m_cap_crq_1.png)

次に `${USER}-cap-crq-2` Project に Pod を作成します。
先ほどと同様にProject: `${USER}-cap-crq-2` に変更し、画面右上の **Create Pod** を選択します。  
![handson](./images/resource_mgmt/create_pod_cap_crq_2.png)

マニフェストを以下のように書き換え  **Create** を選択します。
**`${USER}-` で始まる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
```
apiVersion: v1
kind: Pod
metadata:
  name: quota-cpu-400m
  labels:
    app: httpd
  namespace:  ${USER}-cap-crq-2
spec:
  containers:
    - name: httpd
      image: 'image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest'
      ports:
        - containerPort: 8080
      resources:
        requests:
          cpu: 400m
          memory: 256Mi
        limits:
          cpu: 800m
          memory: 256Mi
```

**Create** を選択すると以下のようなエラーが出力され、`${USER}-cap-crq-2` Project に Pod をまだデプロイしていませんが、Pod をデプロイすることはできません。
`ClusterResourceQuota` を設定することで 2 つの Project にまたがりシステムリソースの上限が設定されていることが分かります。
![handson](./images/resource_mgmt/crq_error.png)

`ClusterResourceQuota` は適用された Project の **ResourceQuota** の画面に表示されます。  
![handson](./images/resource_mgmt/crq_applied_quota.png)

`ClusterResourceQuota` を作成する際、Project 作成時に "Description" に設定した値を条件に Project を判定させました。
一方 Project の `metadata.labels` フィールドと `ClusterResourceQuota` の `spec.selector.matchLabels` フィールドで判定させることもできます。

次の例では `app: frontend` を設定した Project を対象とします。
```
apiVersion: quota.openshift.io/v1
kind: ClusterResourceQuota
metadata:
  name: frontend-quota
spec:
  quota:
    hard:
      requests.cpu: 500m
      limits.cpu: 1
      `Request` : "1Gi
      `Limit` : "2Gi
  selector:
    labels:
      matchLabels:
        app: frontend 
```

Project を作成する際、`metadata.labels` フィールドに `app: frontend` ラベルを指定します。
```
apiVersion: config.openshift.io/v1
kind: Project
metadata:
  name: <YOUR_PROJECT>
  labels:
    app: frontend 
```

このように `ClusterResourceQuota` を用いることで複数の Project にまたがってリソースの上限を設けることができます。
Project を開発や本番環境とみなし複数の Project を対象に共通の設定でリソースを制限する場合や Frontend や Backend なアプリケーションを Project 毎に配置して複数の Project であるシステムを構成する場合などに適用する事ができます。

---
# 3. LimitRange によるシステムリソースの制限
[`Limitrange`](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html/nodes/nodes-cluster-limit-ranges) を用いて、Pod やコンテナが要求する CPU や Memory に対する下限値や上限値を設定します。
`LimitRange` は Pod の CPU や Memory 以外にイメージサイズや `PersistentVolumeClaim` のサイズなど様々な値に設けることができます。

## 3.1. LimitRange の作成
テスト用の Project を作成し、Project に `LimitRange` を設定します。

左側メニューから **Home** → **Project** を選択します。  
![handson](./images/resource_mgmt/menu_project.png)

画面右側の **Create Project** を選択します。  
![handson](./images/resource_mgmt/create_project.png)

Name に `${USER}-cap-limits` を入力し、**Create** を選択します。
**`${USER}-` で始まる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
![handson](./images/resource_mgmt/create_userx_cap_limits.png)

次に `LimitRange` を設定します。
左側メニューから **Administration** → **LimitRanges** を選択します。
この時点ですでに LimitRange が設定されている場合は全ての LimitRange を削除します。
![handson](./images/resource_mgmt/menu_limits.png)

左上の Project が `${USER}-cap-limits` であることを確認して、画面右上の **Create LimitRange** を選択します。
![handson](./images/resource_mgmt/create_limits.png)

マニフェストを以下のように書き換え、**Create** を選択します。
**`${USER}-` で始まる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
```
apiVersion: v1
kind: LimitRange
metadata:
  name: mem-limit-range
  namespace: ${USER}-cap-limits
spec:
  limits:
    - min:
        memory: 256Mi
      max:
        memory: 1Gi
      type: Pod
```

この `LimitRange` は Pod に対する Memory の Limit を最小で 256Mi から最大で 1Gi に指定します。
`limits` フィールドの `min` は `Request` に対する下限値に相当し、`Request` を設定する際に 200Mi の様に 256Mi より低い値を指定することはできません。
一方 `max` は `Limit` の上限値に相当し、`Limit` を設定する際に 2Gi の様に 1Gi よりも大きい値を指定することはできません。
![handson](./images/resource_mgmt/limits_yaml1.png)

**Create** を選択すると `LimitRange` が作成され詳細が表示されます。
スクロールダウンすると設定値を確認する事ができます。

![handson](./images/resource_mgmt/limits_view1.png)  
![handson](./images/resource_mgmt/limits_view2.png)  

## 3.2. Pod の作成
Pod を作成し、`LimitRange` が適用されることを確認します。

左側メニューから **Workloads** → **Pods** を選択します。  
![handson](./images/resource_mgmt/menu_pod.png)

左上の Project が `${USER}-cap-limits` であることを確認して、画面右上の **Create Pod** を選択します。
![handson](./images/resource_mgmt/create_pod_limits.png)

マニフェストを以下のように書き換え、**Create** を選択します。
Memory の `Request`を 512Mi に `Limit` を 1Gi に設定します。
**`${USER}-` で始まる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
```
apiVersion: v1
kind: Pod
metadata:
  name: quota-mem-512mi
  labels:
    app: httpd
  namespace: ${USER}-cap-limits
spec:
  containers:
    - name: httpd
      image: 'image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest'
      ports:
        - containerPort: 8080
      resources:
        requests:
          cpu: 300m
          memory: 512Mi
        limits:
          cpu: 600m
          memory: 1Gi
```
![handson](./images/resource_mgmt/pod_limits_yaml1.png)

**Create** 選択すると Pod は正常にデプロイされます。
これは Pod マニフェストに指定した値が `LimitRange` で定義した値に収まるためです。

では次に下限値を下回る Pod を作成します。
先ほどと同様の手順でマニフェストを以下のように書き換え、**Create** を選択します。
**`${USER}-` で始まる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
```
apiVersion: v1
kind: Pod
metadata:
  name: quota-mem-128mi
  labels:
    app: httpd
  namespace: ${USER}-cap-limits
spec:
  containers:
    - name: httpd
      image: 'image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest'
      ports:
        - containerPort: 8080
      resources:
        requests:
          cpu: 300m
          memory: 128Mi
        limits:
          cpu: 600m
          memory: 1Gi
```

![handson](./images/resource_mgmt/pod_limits_yaml2.png)

**Create** を選択すると以下のようなエラーが出力され、Pod をデプロイすることはできません。
これは `LimitRange` が正しく動作しており、`min` に指定した `memory: 256Mi` に対し、Pod の `Request` は `memory: 128Mi"` と下回り、条件に合致しないためです。
![handson](./images/resource_mgmt/pod_limits_error1.png)

次に以下のようにマニフェストを修正し、再度 **Create** を選択します。
Memory の `Request` を 256Mi に `Limit` を 2Gi に増やします。
**`${USER}-` で始まる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
```
apiVersion: v1
kind: Pod
metadata:
  name: quota-mem-2gi
  labels:
    app: httpd
  namespace: ${USER}-cap-limits
spec:
  containers:
    - name: httpd
      image: 'image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest'
      ports:
        - containerPort: 8080
      resources:
        requests:
          cpu: 300m
          memory: 256Mi
        limits:
          cpu: 600m
          memory: 2Gi
```

![handson](./images/resource_mgmt/pod_limits_yaml3.png)

今度は `LimitRange` の上限値を超えている旨のエラーが表示され、同様に Pod をデプロイすることはできません。
![handson](./images/resource_mgmt/pod_limits_error2.png)


このように `LimitRange` 用いることで Pod 当たりのリソース要求を制限し、ある Pod がシステムリソースを規定以上に要求することを防ぐ事ができます。
`LimitRange` はこれ以外にデフォルトで適用する値を指定する事ができます。

# 4. LimitRange によるデフォルト値の設定
`ResourceQuota` を適用すると Pod やコンテナに `Request` や `Limit` を設定しない場合、デプロイすることはできません。  
そこで `LimitRange` を用いて Pod やコンテナが `Request` や `Limit` でリソースを指定しない場合、規定のデフォルト値を適用させます。

## 4.1. ResourceQuota の作成
テスト用の Project を作成し、Project に `ResourceQuota` を設定します。

左側メニューから **Home** → **Project** を選択します。  
![handson](./images/resource_mgmt/menu_project.png)

画面右側の **Create Project** を選択します。  
![handson](./images/resource_mgmt/create_project.png)

Name に `${USER}-cap-limits-default` を入力し、**Create** を選択します。
![handson](./images/resource_mgmt/create_userx_cap_limits_default.png)

左側メニューから **Administration** → **ResourceQuotas** を選択します。  
![handson](./images/resource_mgmt/menu_resourcequota.png)

左上の Project が `${USER}-cap-limits-default` であることを確認して、画面右上の **Create ResourceQuota** を選択します。
![handson](./images/resource_mgmt/create_resourcequota2.png)

今回はリソースを超えるようなリソースをデプロイしないため、デフォルトの設定のまま **Create** を選択します。
![handson](./images/resource_mgmt/create_rq_example2.png)

`ResourceQuota` が作成されることを確認します。
![handson](./images/resource_mgmt/rq_detail3.png)  

## 4.2. LimitRange の作成
Project に `LimitRange` を設定します。  

左側メニューから **Administration**  →  **LimitRanges** を選択します。
この時点ですでに LimitRange が設定されている場合は全ての LimitRange を削除します。
![handson](./images/resource_mgmt/menu_limits.png)

左上の Project が `${USER}-cap-limits-default` であることを確認して、画面右上の **Create LimitRange** を選択します。
![handson](./images/resource_mgmt/create_limits2.png)

マニフェストを以下のように書き換え、**Create** を選択します。
**`${USER}-` で始まる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
```
apiVersion: v1
kind: LimitRange
metadata:
  name: default-limit-range
  namespace: ${USER}-cap-limits-default
spec:
  limits:
    - defaultRequest:
        cpu: 100m
        memory: 256Mi
      default:
        cpu: 500m
        memory: 512Mi
      type: Container
```

`spec.limits` フィールドの `defaultRequest` は `Request` に対するデフォルト値で `default` は `Limit` のデフォルト値になります。
次に `type: Container` を設定しており、これは Pod ではなくコンテナを対象とします。
![handson](./images/resource_mgmt/limits_yaml2.png)

**Create** を選択すると `LimitRange` が作成され詳細が表示されます。
スクロールダウンすると設定値を確認する事ができます。
![handson](./images/resource_mgmt/limits_default_view1.png)  
![handson](./images/resource_mgmt/limits_default_view2.png)  

## 4.3. Pod の作成
Pod をデプロイし `LimitRange` によるデフォルト値がコンテナに適用されることを確認します。

左側メニューから **Workloads** → **Pods** を選択します。
![handson](./images/resource_mgmt/menu_pod.png)

左上の Project が `${USER}-cap-limits-default` であることを確認し、画面右上の **Create Pod** を選択します。  
![handson](./images/resource_mgmt/create_pod_limits_default.png)  

**Create** を選択します。
![handson](./images/resource_mgmt/pod_limits_default_yaml.png)  
このマニフェストに `Request` や `Limit` が定義されていないため、`ResourceQuota` が設定されている Project ではデプロイできないはずです。

Pod が正常にデプロイされます。
![handson](./images/resource_mgmt/pod_limits_default_detail.png)  

**YAML** を選択すると 131 行目あたり (`containers`) でコンテナの `Request` と `Limit` に `LimitRange` で指定した値が適用されています。
![handson](./images/resource_mgmt/pod_limits_default_yaml_view.png)  

なお CPU や Memory の実際の使用量と `Request` や `Limit` を比較する場合、Pod の **Metrics** から参照することができます。
![handson](./images/resource_mgmt/pod_limits_default_metrics_view1.png)  
![handson](./images/resource_mgmt/pod_limits_default_metrics_view2.png)  

この様に `LimitRange` を設定することで Pod を作成する際に CPU や Memory に対し設定し忘れることを防ぐ、また規定の下限値や上限値を超えてリソースを要求することを防ぐことができます。

以上でこのセクションは終了です。
